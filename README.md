PRENAULT SWISS ARMY KNIFE
-------------------------

The main feature of this box is that it's built to develop multiple sites on, which should save us a lot of disk space and setup time for each project. This is done through with the Vesta control panel and some DNS magic.

GETTING STARTED
---------------
    Install VirtualBox and vagrant.
    $ git clone git@gitlab.com:m-bender/Prenault_SAK.git
    $ vagrant up
    Open sak.prenault.com in your browser.
    Read through the links and login credentials on this page.

Note: It make take a minute or so before Apache finishes loading after boot.

CREATING A WEBSITE
------------------
    Open up the Vesta Control Panel.
        https://sak.prenault.com:8083
        or https://192.168.33.10:8083
    Credentials
        Username: admin
        Password: admin
    Click web in the top control panel.
    Click the big green plus button.
    Add a subdomain of sak.prenault.com
        Example: mysite.sak.prenault.com
    Visit that subdomain in your browser.

Note: sak.prenault.com and it's subdomains will resolve to your machine locally.

PERFORMANCE NOTES
------------------
Vagrant's performance on Windows out of the box is well... not ideal. The major source of problems comes from VirtualBox's shared folders feature. While super convenient, it is slooow. Here are some options:

    Turn off file sharing (recommended)
    Comment out line #15 in the Vagrantfile. If you do this, you will need to either ssh into the vagrant box and use a command line text editor like Vim OR use an IDE/text editor on Windows that supports ftp sync. Netbeans has this feature out of the box and is a great option.

    Use Samba file sharing
    Comment out line #15 and uncomment line #22 in the Vagrantfile. Before this will work, you will also need to install VirtualBox guest additions in the box.

NERD STATS
-----------
I ran some performance tests to compare the above options. Keep in mind this is on my laptop with an SSD.

    Shared Folders Turned Off
    -------------------------------------------------
    Zero File Read
    81920000 bytes (82 MB) copied, 0.288434 s, 284 MB/s
    Random File Read
    81920000 bytes (82 MB) copied, 0.30921 s, 265 MB/s

    Default Shared Folder
    -------------------------------------------------
    Zero File Read
    81920000 bytes (82 MB) copied, 59.7499 s, 1.4 MB/s
    Random File Read
    81920000 bytes (82 MB) copied, 46.8803 s, 1.7 MB/s

    NFS Shared Folder
    -------------------------------------------------
    Zero File Read
    81920000 bytes (82 MB) copied, 59.2599 s, 1.4 MB/s
    Random File Read
    81920000 bytes (82 MB) copied, 55.5099 s, 1.5 MB/s

    Samba Shared Folder
    --------------------------------------------------
    Zero File Read
    81920000 bytes (82 MB) copied, 9.1561 s, 8.9 MB/s
    Random File Read
    81920000 bytes (82 MB) copied, 11.6633 s, 7.0 MB/s